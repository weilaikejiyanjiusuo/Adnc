﻿namespace Adnc.Shared.Application.Contracts.Services;

public interface IAdncServiceCollection
{
    public void AddAdncServices();
}