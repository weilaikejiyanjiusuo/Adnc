﻿global using Adnc.Cus.Application.Contracts.Dtos;
global using Adnc.Shared.Application.Contracts.Attributes;
global using Adnc.Shared.Application.Contracts.Dtos;
global using Adnc.Shared.Application.Contracts.Services;
global using Adnc.Shared.ResultModels;
global using FluentValidation;
global using System.Threading.Tasks;