﻿namespace Adnc.Infra.EfCore
{
    public static class EfCoreConsts
    {
        public const string MyCAT_ROUTE_TO_MASTER = "#mycat:db_type=master";
        public const string MAXSCALE_ROUTE_TO_MASTER = "maxscale route to master";
    }
}